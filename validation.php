<?php
// Use arrays to store the infor of gender and major
$gender_arr = array("Nam", "Nữ");
$major_arr = array("Khoa học máy tính"=>"MAT", "Khoa học vật liệu"=>"KDL");	
		
$name = $gender = $major = $birth = $add = "";
$nameErr = $genderErr = $majorErr = $addressErr = "";
		
function get_data($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
		
session_start();
			
if(isset($_POST["submit"])) {
	if(empty(get_data($_POST["name"]))) {
		$nameErr = "Hãy nhập tên";
	} else {
		$name = get_data($_POST["name"]);
	}
				
	if(empty($_POST["gender"])) {
		$genderErr = "Hãy chọn giới tính";
	} else {
		$gender = get_data($_POST["gender"]);
	}
					
	if(empty($_POST["major"])) {
		$majorErr = "Hãy chọn phân khoa";
	} else {
		$major = array_search(get_data($_POST["major"]), $major_arr);
	}
				
	if(empty($_POST["birth"])) {
		$addressErr = "Hãy nhập ngày sinh";
	} else {
			$add = array_search(get_data($_POST["add"]), $major_arr);
	}
				
	if ($name != "" && $gender != "" && $major != "" && $add !="") {
		$_SESSION["name"] = $name;
		$_SESSION["gender"] = $gender;
		$_SESSION["major"] = $major;
		$_SESSION["birth"] = $birth;
		$_SESSION["add"] = $add;
		
		header("location: do_regist.php");
		exit();
	}
}			
?>