<!DOCTYPE html>
<html lang="vi">
	<head>
		<title>Result</title>
		<link rel="stylesheet" type="text/css" href="form.css?v=<?php echo time(); ?>">
		<?php   
			header('Content-Type: text/html; charset=utf-8');
			date_default_timezone_set("Asia/Bangkok");
			session_start();
		?>
		<style>
			.container {
				padding: 1.5% 3.5% 1% 1.5%;
				height: 160px;
			}
			
			.col-infor {
				margin-bottom: 0px;
				height: 34px;
			}
			
			.col-infor p {
				border: none;
				margin-top: 0px;
				margin-bottom: 0px;
				height: 16px;
				padding-top: 12px;
				
			}
		</style>
	</head>
	
	<body>
		<div class="container">
			<div class="form-group">
				<div class="col-label">
					<label>Họ và tên<strong>*</strong></label>
				</div>
			
				<div class="col-infor">
					<p><?php echo $_SESSION["name"]; ?></p> 
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-label">
					<label>Giới tính<strong>*</strong></label>
				</div>
			
				<div class="col-infor">
					<p><?php echo $_SESSION["gender"]; ?></p> 
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-label">
					<label>Phân khoa<strong>*</strong></label>
				</div>
			
				<div class="col-infor">
					<p><?php echo $_SESSION["major"]; ?></p> 
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-label">
						<label>Ngày sinh<strong>*</strong></label>
					</div>
			
				<div class="col-infor">
					<p><?php echo $_SESSION["birth"]; ?></p> 
				</div>
			</div>

			<div class="form-group">
				<div class="col-label">
					<label>Địa chỉ</label>
				</div>
			
				<div class="col-infor">
					<p><?php echo $_SESSION["add"]; ?></p> 
				</div>
			</div>

		</div>
		
		<?php session_destroy(); ?>
	</body>
</html>