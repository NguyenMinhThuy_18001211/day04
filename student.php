<!DOCTYPE html>
<html lang="vi">
	<head>
		<title>Register</title>
		<link rel="stylesheet" type="text/css" href="form.css?v=<?php echo time(); ?>">
		<?php   
			header('Content-Type: text/html; charset=utf-8');
			date_default_timezone_set("Asia/Bangkok");
		?>
	</head>
	
	<body>
		<?php include "validation.php"; ?>
		
		<!-- Display -->
		<div class="container">
			<div class="form-validate">
				<p><?php echo $nameErr; ?></p>
				<p><?php echo $genderErr; ?></p>
				<p><?php echo $majorErr ?></p>
				<p><?php echo $addressErr ?></p>
			</div>
		
			<form method="post">
				<div class="form-group">
					<div class="col-label">
						<label for="name">Họ và tên <strong>*</strong></label>
					</div>
				
					<div class="col-input">
						<input type="text" id="name" name="name" value="<?php echo isset($_POST["name"]) ? $_POST["name"] : "" ?>" /> 
					</div>
				</div>
			
			
				<div class="form-group">
					<div class="col-label">
						<label>Giới tính <strong>*</strong></label>
					</div>
				
					<div class="col-input">
						<div class="radio-group">
							<?php
							// Use "for" to display gender
							for($i = count($gender_arr) - 1; $i >= 0; $i--) {
								echo "<div class=\"radio-item\">";
								if (isset($_POST["gender"]) && $_POST["gender"] == $gender_arr[$i]) {
									echo"<input type=\"radio\" name=\"gender\" value=$gender_arr[$i] checked />";
								} else {
									echo"<input type=\"radio\" name=\"gender\" value=$gender_arr[$i] />";
								}
								echo"<label class=\"radio-item\">$gender_arr[$i]</label>";
								echo "</div>";
							}
							?>
						</div>
					</div>
				</div>
	
			
				<div class="form-group">
					<div class="col-label">
						<label for="major">Phân khoa <strong>*</strong></label>
					</div>
				
					<div class="col-input">
						<select id="major" name="major">
							<option hidden disabled selected value>&nbsp;</option>
							<?php
							// Use "foreach" to display major
							foreach($major_arr as $x => $x_value) {
								if (isset($_POST["major"]) && $_POST["major"] == $x_value) {
									echo"<option value=$x_value selected>$x</option>";;
								} else {
									echo"<option value=$x_value>$x</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<div class="col-label">
						<label for="birth">Ngày sinh <strong>*</strong></label>
					</div>
				
					<div class="col-input">
						<input style="width:52%;" type="text" id="birth" name="birth" value="<?php echo isset($_POST["birth"]) ? $_POST["birth"] : "" ?>" /> 
					</div>
				</div>

				<div class="form-group">
					<div class="col-label">
						<label for="add">Địa chỉ </label>
					</div>
				
					<div class="col-input">
						<input type="text" id="add" name="add" value="<?php echo isset($_POST["add"]) ? $_POST["add"] : "" ?>" /> 
					</div>
				</div>
				
				
				<div class="submit">
					<button type="submit" name="submit" value="Submit">Đăng ký</button>
				</div>
		    </form>
		</div>
		<script>
			
		</script>
		
		<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
	</body>
</html>